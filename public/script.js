// archivo: scripts.js
var request = new XMLHttpRequest()

request.open('GET', 'https://jsonplaceholder.typicode.com/users', true)
request.onload = function() {
  // Begin accessing JSON data here
  var respuesta = JSON.parse(this.response)
  var app = document.getElementById('root')

  if (this.status >= 200 && this.status < 400) {


    respuesta.forEach(function(objeto, indice) {

      var contenedor = document.createElement('div')

      contenedor.classList.add('contacto')


      var nombre = document.createElement('h2')
      nombre.innerHTML = 'Name: ' + objeto.name
      nombre.classList.add('contacto__nombre')
      contenedor.appendChild(nombre)


      var empresa = document.createElement('h4')
      empresa.innerHTML = 'Company Name: ' + objeto.company.name
      empresa.classList.add('contacto__empresa')
      contenedor.appendChild(empresa)


      var img = document.createElement('img')
      img.src = 'http://i.pravatar.cc/100'
      var avatar = document.createElement('div')
      avatar.classList.add('contacto__avatar')
      contenedor.appendChild(avatar)
      avatar.appendChild(img)

      var card = document.createElement('div')
      card.classList.add('contacto__card')
      contenedor.appendChild(card)

      var ac = document.createElement('div')
      ac.classList.add('acordeon__titulo')
      contenedor.appendChild(ac)
      ac.innerHTML = objeto.id

      var aco = document.createElement('div')
      aco.classList.add('acordeon__contenido')
      contenedor.appendChild(aco)


      var p1 = document.createElement('p')
      var p2 = document.createElement('p')
      var p3 = document.createElement('p')
      var p4 = document.createElement('p')
      var p5 = document.createElement('p')
      var p6 = document.createElement('p')
      var p7 = document.createElement('p')
      var p8 = document.createElement('p')
      var p9 = document.createElement('p')
      var p10 = document.createElement('p')
      var p11 = document.createElement('p')
      var p12 = document.createElement('p')

      p1.textContent = 'Email: ' + objeto.email
      p2.textContent = 'Username: ' + objeto.username
      p3.textContent = 'Street: ' + objeto.address.street
      p4.textContent = 'Suite: ' + objeto.address.suite
      p5.textContent = 'City: ' + objeto.address.city
      p6.textContent = 'Zipcode: ' + objeto.address.zipcode
      p7.textContent = 'Lat: ' + objeto.address.geo.lat
      p8.textContent = 'Lng: ' + objeto.address.geo.lng
      p9.textContent = 'Phone: ' + objeto.phone
      p10.textContent = 'Website: ' + objeto.website
      p11.textContent = 'Catch Phrase: ' + objeto.company.catchPhrase
      p12.textContent = 'Bs: ' + objeto.company.bs

      aco.appendChild(p1)
      aco.appendChild(p2)
      aco.appendChild(p3)
      aco.appendChild(p4)
      aco.appendChild(p5)
      aco.appendChild(p6)
      aco.appendChild(p7)
      aco.appendChild(p8)
      aco.appendChild(p9)
      aco.appendChild(p10)
      aco.appendChild(p11)
      aco.appendChild(p12)

      app.appendChild(contenedor)

    })
  }
  $('.acordeon__titulo').click(function() {
    $(this).toggleClass('acordeon--activo').next().slideToggle()
  })
}
request.send()
